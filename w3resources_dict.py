#1. Python script to sort (ascending and descending)
# a dictionary by value.

check = {
    1: 2,
    3: 4,
    4: 3,
    2: 1,
    0: 0
}

# print(sorted(check)) #print keys sorted asc order
# print(sorted(check.items(), key= lambda x:x[1])) #print vals sorted in asc order
# print(sorted(check.items(), reverse = True, key= lambda x:x[1])) #prints vals sorted in dsc order

#2. Write a Python script to add a key to a dictionary.
# dct = {0: 10, 1: 20}
#
# def add_item_to_dict(dct, addition):
#     dct.update(addition)
#     return dct
#
# print(add_item_to_dict(dct, {2:30}))

#3. Write a Python script to concatenate following
# dictionaries to create a new one.
# dct1={
#     1:10,
#     2:20
# }
# dct2={
#     3:30,
#     4:40
# }
# dct3={
#     5:50,
#     6:60
# }
#
# def concat_dict(dct1,dct2,dct3):
#     newdct = {}
#     for d in (dct1,dct2,dct3): newdct.update(d)
#     return newdct
#
# print(concat_dict(dct1,dct2,dct3))

#4. Write a Python script to check whether a given
# key already exists in a dictionary.

# def does_key_exist(dct, key):
#     val = dct.get(key)
#     if (val == None): return("Doesn't exist.")
#     return val
#
# dct = {
#   "brand": "Ford",
#   "model": "Mustang",
#   "year": 1964
# }
# print(does_key_exist(dct, "t"))

#5. Write a Python program to iterate over
# dictionaries using for loops.

# def dct_for_loop_iter(dct):
#     for k,v in dct.items():
#         print(k,v)
#
# dct = {'x': 10, 'y': 20, 'z': 30}
# dct_for_loop_iter(dct)

#6. Write a Python script to generate
# and print a dictionary that contains
# a number (between 1 and n) in the form (x, x*x)

# def gen_dict(n):
#     dct = {}
#     for number in range(1,n+1):
#         dct.update({number : number*number})
#     return dct
#
# print(gen_dict(5))'

#10. Write a Python program to sum
# all the items in a dictionary.

# def sum_dict_items(dct):
#     return sum(dct.values())
#
# dct= {'data1':100,'data2':-54,'data3':247}
# print(sum_dict_items(dct))

#11. Write a Python program to multiply
# all the items in a dictionary.

# def mltpy_dict(dct):
#     tot_val = 1
#     for v in dct.values():
#         tot_val *= v
#     return tot_val
#
# dct = {'data1':100,'data2':-54,'data3':247}
# print(mltpy_dict(dct))

#12. Write a Python program to remove a
# key from a dictionary.

# def remove_key(dct, key):
#     # dct.pop(key)
#     del dct[key]
#     return dct
#
# dct = {'a':1,'b':2,'c':3,'d':4}
# print(remove_key(dct, 'b'))

# 13. Write a Python program to map two
# lists into a dictionary.

# def lists_to_dct(keys, values):
#     dct = dict(zip(keys, values))
#     return dct
#
# keys = ['red', 'green', 'blue']
# values = ['#FF0000','#008000', '#0000FF']
# print(lists_to_dct(keys, values))

#14. Write a Python program to sort
# a dictionary by key.

# def sort_dct(dct):
#     newdct = {}
#     for key in sorted(dct):
#         newdct.update({key: dct[key]})
#         # print (f"{key}:{dct[key]}")
#     return newdct
#
# dct = {
#     'red':'#FF0000',
#     'green':'#008000',
#     'black':'#000000',
#     'white':'#FFFFFF'
# }
# print(sort_dct(dct))

#15. Write a Python program to get the
# maximum and minimum value in a dictionary.
# def max_min_val(dct):
#     newdct = {}
#     min_val = min(dct.values())
#     max_val = max(dct.values())
#     return(f"min_val: {min_val}\nmax_val: {max_val}")
# dct = {'x':500, 'y':5874, 'z': 560}
# print(max_min_val(dct))

#16. Write a Python program to get a
# dictionary from an object's fields.
# class Person:
#     def __init__(self):
#         self.height = '6f4'
#         self.weight = '95kg'
#         self.hair_color = 'black'
#
#     def jump(self):
#         pass
#
# test = Person()
# print(test.__dict__)

#17. Write a Python program to remove
# duplicates from Dictionary.
# def remove_dup(dct):
#     result = {}
#     for key, val in dct.items():
#         if val not in result.values():
#             result[key] = val
#     print(len(dct), len(result))
#     return result
#
#
# dct = {'id1':
#    {'name': ['Sara'],
#     'class': ['V'],
#     'subject_integration': ['english, math, science']
#    },
#  'id2':
#   {'name': ['David'],
#     'class': ['V'],
#     'subject_integration': ['english, math, science']
#    },
#  'id3':
#     {'name': ['Sara'],
#     'class': ['V'],
#     'subject_integration': ['english, math, science']
#    },
#  'id4':
#    {'name': ['Surya'],
#     'class': ['V'],
#     'subject_integration': ['english, math, science']
#    },
# }
# print(remove_dup(dct))

#18. Write a Python program to check a
# dictionary is empty or not.
# def is_dict_empty(dct):
#     if (len(dct) == 0): return("Dictionary is empty")
#     else: return dct
#
# dct = {1:2}
# print(is_dict_empty(dct))

#19. Write a Python program to combine two
# dictionary adding values for common keys.
# def add_two_dicts(dct1, dct2):
#     newdct = dct1.copy()
#     newdct.update(dct2)
#     for i,j in dct1.items():
#         for x,y in dct2.items():
#             if i == x: newdct[i] = j + y
#     return newdct
#
#
# d1 = {'a': 100, 'b': 200, 'c':300}
# d2 = {'a': 300, 'b': 200, 'd':400}
# print(add_two_dicts(d1, d2))

#20. Write a Python program to print
# all unique values in a dictionary.

# data = [
#     {"V":"S001"},
#     {"V": "S002"},
#     {"VI": "S001"},
#     {"VI": "S005"},
#     {"VII":"S005"},
#     {"V":"S009"},
#     {"VIII":"S007"}
# ]
#
# def uniq_values(data):
#     uniq_set = set()
#     # print(data[0]) #index in list
#     # print(data[0]['V']) #reach the value within the list
#     # print({data[0]['V'], data[1]['V']})
#     for i in range(len(data)):
#         for k, v in data[i].items():
#             if v not in uniq_set:
#                     uniq_set.add(v)
#     return uniq_set
#
# print(uniq_values(data))

#21. Write a Python program to create and display
# all combinations of letters, selecting each letter
# from a different key in a dictionary.
# d = {'1':['a','b'], '2':['c','d'], '3':['e','f']}
#
# def value_combos(d):
#     import itertools
#     for combo in itertools.product(*[d[k] for k in sorted(d.keys())]):
#         print(''.join(combo))
#
# value_combos(d)

#22. Write a Python program to find the
# highest 3 values in a dictionary.
# def highest_3_vals(dct):
#     highest_3_val = sorted(dct.items(), key= lambda x: x[1], reverse=True)[:3]
#     return highest_3_val
# dct = {'a':500, 'b':5874, 'c': 560,'d':400, 'e':5874, 'f': 20}
# print(highest_3_vals(dct))
# print(dct.items())

#23. Write a Python program to combine values
# in python list of dictionaries.
# def aggr_vals(data):
#     list_items = []
#     dict = {}
#     for items in data:  #generate list of items
#         # print(items)
#         # print(items['item'])
#         if items['item'] not in dict.keys(): dict.update({items['item']: items['amount']})
#         else: dict[items['item']] += items['amount']
#
#     return dict
#
# data = [
#     {'item': 'item1', 'amount': 400},
#     {'item': 'item2', 'amount': 300},
#     {'item': 'item1', 'amount': 750}
# ]
#
# print(aggr_vals(data))

#24. Write a Python program to create
# a dictionary from a string.
# def dict_from_string(samp):
#     dict = {}
#     for ch in samp:
#         if ch in dict.keys(): dict[ch] += 1
#         else: dict.update({ch:1})
#     return dict
#
#
# samp = 'hello world'
# print(dict_from_string(samp))

#25. Write a Python program to print
# a dictionary in table format.

# def dict_in_table_format(dict):
#     for each_row in zip(*([i] + (j)
#         for i, j in dict.items())):
#             print(*each_row, " ")
#
# dict = {'C1':[1,2,3],'C2':[5,6,7],'C3':[9,10,11]}
# dict_in_table_format(dict)

#26. Write a Python program to count
# the values associated with key in a dictionary.
# data = [
#     {'id': 1, 'success': True, 'name': 'Lary'},
#     {'id': 2, 'success': False, 'name': 'Rabi'},
#     {'id': 3, 'success': False, 'name': 'Alex'}
# ]
#
# def count_item_dict(data):
#     count_true = 0
#     for dict in data:
#         if dict['success'] == True: count_true += 1
#     return count_true
#
# print(count_item_dict(data))

#27. Write a Python program to convert a list
# into a nested dictionary of keys.
# def nested_dict(data):
#     new_dict = current = {}
#     for item in data:
#         current[item] = {}
#         current = current[item]
#     print(new_dict)
#
# data = [1,2,3,4]
# nested_dict(data)

#28. Write a Python program to sort
# a list alphabetically in a dictionary.
# def sort_list_dict(dict):
#     sorted_dict = {x: sorted(y) for x, y in dict.items()}
#     return sorted_dict
#
# dict = {'n1': [2, 3, 1], 'n2': [5, 1, 2], 'n3': [3, 2, 4]}
# print(sort_list_dict(dict))

#29. Write a Python program to remove spaces
# from dictionary keys.
# def rem_space_dict_keys(dict):
#     spc_removed = {k.translate({32: None}) : v for k, v in dict.items()}
#     return spc_removed
#
# dict = {'S  001': ['Math', 'Science'], 'S    002': ['Math', 'English']}
# print(rem_space_dict_keys(dict))