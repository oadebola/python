spam = ['apples', 'bananas', 'tofu', 'cats']

def list_stringify(list):
    string = ""
    if len(list) == 0: print("list is empty")
    for i in range(len(list)):
        if i == (len(list) - 1):
            string += "and " + list[i]
            break
        string += list[i] + ", "
    print(string)

list_stringify(spam)
