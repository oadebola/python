def printTable(L_string):
    colWidths = [0] * len(L_string)
    table = ""
    i, j = 0, 0

    for x in range(len(L_string)):                  # find the longest word for each column and set to the column
        for y in range(len(L_string[x])):
            if len(L_string[x][y]) > colWidths[x]:
                colWidths[x] = len(L_string[x][y])

    while j < len(L_string[0]):                     #add word into column right justified to the longest word
        while i < len(L_string):
            word = L_string[i][j]
            table += (word.rjust(colWidths[i]) + " ")
            i += 1
            if i == len(L_string):
                table += "\n"
        i = 0
        j += 1

    return table



tableData = [
    ['apples', 'oranges', 'cherries', 'banana'],
    ['Alice', 'Bob', 'Carol', 'David'],
    ['dogs', 'cats', 'moose', 'goose']
]
print(printTable(tableData))