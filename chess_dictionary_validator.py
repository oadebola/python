"""
Write a function named isValidChessBoard() that takes a dictionary argument
and returns True or False depending on if the board is valid.
A valid board will have exactly one black king and exactly one white king.
Each player can only have at most 16 pieces, at most 8 pawns,
and all pieces must be on a valid space from '1a' to '8h';
that is, a piece can’t be on space '9z'.
The piece names begin with either a 'w' or 'b' to represent white or black,
followed by 'pawn', 'knight', 'bishop', 'rook', 'queen', or 'king'.
This function should detect when a bug has resulted in an improper chess board.
"""

chess_board = {
    '1h': 'wking',
    '6c': 'wqueen',
    '2g': 'bbishop',
    '5h': 'bqueen',
    '3e': 'bking',
    '4b': 'wrook'
}

def isValidChessBoard(chess_board):
    valid = True

    #debug individual functions
    # if (w_b_king_count(chess_board) != True): valid = False
    # if (each_player_max_16_pieces(chess_board) != True): valid = False
    # if (validity_of_pieces(chess_board) != True): valid = False
    # if (no_more_than_8_pawns(chess_board) != True): valid = False
    # if (piece_name(chess_board) != True): valid = False

    #all conditions must be true for chess board to be valid
    if (
            (
            w_b_king_count(chess_board))
            and (each_player_max_16_pieces(chess_board))
            and (validity_of_pieces(chess_board))
            and (no_more_than_8_pawns(chess_board))
            and (piece_name(chess_board)) != True
    ): valid = False

    return valid

def w_b_king_count(chess_board):
    #counting the number of w/b king
    bking, wking = 'bking', 'wking'
    bkcnt, wkcnt = 0, 0
    arr_chess_board_vals = list(chess_board.values())
    answer = True
    for i in range(len(arr_chess_board_vals)):
        watch = arr_chess_board_vals[i]
        if arr_chess_board_vals[i] == bking:
            bkcnt += 1
            if bkcnt > 1:
              answer = False
              break
        elif arr_chess_board_vals[i] == wking:
            wkcnt += 1
            if wkcnt > 1:
                answer = False
                break
        else:
            continue
    return answer
# print(w_b_king_count(chess_board))

def each_player_max_16_pieces(chess_board):
    # Each player must have at most 16 pieces
    arr_chess_board_vals = list(chess_board.values())
    num_black_pieces, num_white_pieces = 0, 0
    answer1 = True
    for j in range(len(arr_chess_board_vals)):
        b_or_w_piece = arr_chess_board_vals[j][0]
        if b_or_w_piece == 'b':
            num_black_pieces += 1
            if num_black_pieces > 16:
                answer1 = False
                break
            else: continue
        elif b_or_w_piece == 'w':
            num_white_pieces += 1
            if num_white_pieces > 16:
                answer1 = False
                break
            else: continue
        else:
            continue
    return answer1
# print(each_player_max_16_pieces(chess_board))

def validity_of_pieces(chess_board):
    # checking validity of keys
    arr_chess_board_keys = list(chess_board.keys())
    possible_chess_number_places = ["1","2","3","4","5","6","7","8"]
    possible_chess_letter_places = ["a","b","c","d","e","f","g","h"]
    answer3 = True
    for k in range(len(arr_chess_board_keys)):
        chess_letter_piece = arr_chess_board_keys[k][1]
        chess_number_piece = arr_chess_board_keys[k][0]
        if (chess_letter_piece in possible_chess_letter_places) and (chess_number_piece in possible_chess_number_places):
            answer3 = True
        else:
            answer3 = False
            break
    return answer3
# print(validity_of_pieces(chess_board))

def no_more_than_8_pawns(chess_board):
    arr_chess_board_vals = list(chess_board.values())
    wpawn, bpawn = 0,0
    answer4 = True
    for l in range(len(arr_chess_board_vals)):
        if arr_chess_board_vals[l][0] == 'w' and arr_chess_board_vals[l][1:] == 'pawn':
            wpawn += 1
            if wpawn > 8:
                answer4 = False
                break
            else: continue
        elif arr_chess_board_vals[l][0] == 'b' and arr_chess_board_vals[l][1:] == 'pawn':
            bpawn += 1
            if bpawn > 8:
                answer4 = False
                break
            else:
                continue
    return answer4
# print(no_more_than_8_pawns(chess_board))

def piece_name(chess_board):
    arr_piece_name = ['pawn', 'knight', 'bishop', 'rook', 'queen', 'king']
    arr_chess_board_vals = list(chess_board.values())
    answer5 = True
    for m in range(len(arr_chess_board_vals)):
        if arr_chess_board_vals[m][1:] not in arr_piece_name:
            answer5 = False
            break
        else: continue
    return answer5
# print(piece_name(chess_board))

print(isValidChessBoard(chess_board))