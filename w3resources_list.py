### w3resource python data type: List - Exercises, Practice, Solution

### 1.Sum all the items in a list

# def sum_list(list):
#
#     total = 0
#
#     for number in list:
#         total += number
#
#     return total
#
#
# list = [1,2,-8]
# print(sum_list(list))

### 2.Multiply all items in a list

# def multiply_list(list):
#
#     total = 1
#
#     for number in list:
#         total *= number
#
#     return total
#
# list = [1,2,-8]
# print(multiply_list(list))

### 3.get the largest number from a list

# def largest_number(list):
#     largest = float("-inf")
#
#     for number in list:
#
#         if number > largest:
#             largest = number
#
#     return largest
#
# list = [1,2,-8]
# print(largest_number(list))

### 4.get the smallest number from a list
# def smallest_number(list):
#     smallest = float("+inf")
#
#     for number in list:
#
#         if number < smallest:
#             smallest = number
#
#     return smallest
#
# list = [1,2,-8]
# print(smallest_number(list))

# 5. count the number of strings where the length is 2
# or more and the first and last character are same
# from a given list of strings.


# def str_length_2_more_last_ch_same_first_ch(list):
#     count = 0
#     if len(list) == 0: print("no list presented, count: ", count)
#     for string in list:
#         if len(string) < 2: continue
#         else:
#             if string[0] == string[-1]: count += 1
#     return count
#
# list = ['abc', 'xyz', 'aba', '1221', 'aba']
# print(str_length_2_more_last_ch_same_first_ch(list))


# 6. get a list, sorted in increasing order
# by the last element in each tuple from a given
# list of non-empty tuples.

# def lst_elem_tuple_sort(list):
#     if len(list) == 0: print("no list presented")
#     else:
#         list.sort(key= lambda x: x[-1])
#         return list
#
# list = [(2, 5), (1, 2), (4, 4), (2, 3), (2, 1)]
# print(lst_elem_tuple_sort(list))

# 7.remove duplicates from a list
# def remove_duplicate(lst):
#     lst = list(dict.fromkeys(lst))
#     return lst
#
# lst = ["a", "b", "a", "c", "c"]
# print(remove_duplicate(lst))

# 8. check if list is empty or not
# def is_list_empty(list):
#     if len(list) == 0:
#         print("This list is empty")
#     else:
#         print("There seems to be something in here")
#
# list = []
# print(is_list_empty(list))

# 9. clone or copy a list
# def copy_list(list):
#     copy_list = list.copy()
#     return copy_list

# 10. find the list of words that are longer
# than n from a given list of words
# def longer_than_n(list, n):
#     count = 0
#     newlist = []
#     if len(list) == 0: return "list empty"
#     else:
#         for word in list:
#             if len(word) >= n:
#                 count += 1
#                 newlist.append(word)
#         return (newlist, count)
#
# list = ['quick', 'brown', 'jumps', 'over', 'lazy']
# print(longer_than_n(list, 5))

# 11.takes two lists and returns True
# if they have at least one common member.
# def list_common_number(list1, list2):
#     for item1 in list1:
#         for item2 in list2:
#             if item1 == item2:
#                 return True
#                 break
#             else: continue
#     return False
# list1 = [1,2,3,4,5]
# list2 = [5,6,7,8,9]
# print(list_common_number(list1, list2))

# 12. print specified list items after removing the 0th, 4th and 5th
# def list_edit(list):
#     newlist = []
#     for i in range(len(list)):
#         if i == 0 or i == 4 or 1 == 5:
#             continue
#         else:
#             newlist.append(list[i])
#     return newlist
#
# list = ['Red', 'Green', 'White', 'Black', 'Pink', 'Yellow']
# print(list_edit(list))

#13. Generate a 3D array
# def three_d_array(r, c, d):
#     grid = []
#     x,y,z = 0,0,0
#     for x in range(r):
#         grid.append([])
#         for y in range(c):
#             grid[x].append([])
#             for z in range(d):
#                 grid[x][y].append('*')
#                 z += 1
#             y += 1
#             z = 0
#             if y == range(c):
#                 x =+ 1
#                 y = 0
#
#     for x in range(r):
#         for y in range(c):
#             print(grid[x][y])
#         print("\n")
#
#
# three_d_array(3,4,6)

#14. print the numbers of a specified list
# def remove_even(list):
#     newlist = []
#     for item in list:
#         if item % 2 != 0:
#             newlist.append(item)
#     return newlist
#
# list = [7 , 8, 120, 25, 44, 20, 27]
# print(remove_even(list))

#15. Shuffle and print a specified list
# from random import shuffle
#
# list = ['Red', 'Green', 'White', 'Black', 'Pink', 'Yellow']
# shuffle(list)

#16. Write a Python program to generate and print a list
# of first and last 5 elements where the values are
# square of numbers between 1 and 30 (both included).

# def list_print_firstnlast_elem():
#     list = []
#     for i in range(1,31):
#         list.append(i**2)
#     # return list
#     print(list[:5])
#     print(list[-5:])
#
# print(list_print_firstnlast_elem())

#17. Generate and print a list except for the first 5
# elements, where the values are square of numbers
# between 1 and 30 (both included).

# def list_gen_squares():
#     list = []
#     for i in range(1,31):
#         if i < 6:
#             continue
#         else:
#             list.append(i**2)
#     return list
#
# print(list_gen_squares())

#18.generate all permutations of a list in Python
# import itertools
# print(list(itertools.permutations([1,2,3])))

#19. Get the difference between the two lists
# def diff_two_lists(l1, l2):
#     l1 = list(dict.fromkeys(l1))
#     l2 = list(dict.fromkeys(l2))
#     l3 = []
#     for item in l1:
#         if item not in l2:
#             l3.append(item)
#     print(l1, l2, l3)
#
# l1 = [1, 2, 3, 4]
# l2 = [1, 2]
# diff_two_lists(l1, l2)

#20. Write a Python program to access the index of a list.
# def return_item_index(list, n):
    # Only use one of these at a time
    # if n > (len(list) - 1):
    #     return ('index out of range')
    # else:
    #     return list[n] or []

#     for num_index, num_val in enumerate(list):
#         print(num_index, num_val)
#
# list = [5, 15, 35, 8, 98]
# print(return_item_index(list, 4))

#21. Write a Python program to convert
# a list of characters into a string.
# def list_ch_string(list):
    # Only use one of these at a time

    # string = ''
    #
    # for item in list:
    #     string += item
    # return string

    # string = ''.join(list)
    # return string

# list = ['a', 'b', 'c', 'd']
# print(list_ch_string(list))

#22.find the index of an item in a specified list.
# def return_index_item_list(list, item):
#     if item not in list:
#         return (item, 'not in list')
#     else:
#         return (list.index(item))
#
# list = [10, 30, 4, -6]
# print(return_index_item_list(list, 7))

#23. Python program to flatten a shallow list.
# def flatten_shallow_list(lst):
    # Only use one of these at a time

    # arr, itm, newlist = 0, 0, []
    # for arr in range(len(list)):
    #     for item in list[arr]:
    #         newlist.append(item)
    #         item += 1
    #     arr += 1
    # return (newlist)

    # lst = [[2,4,3],[1,5,6],[9],[7,9,0]]
    # newlist = []
    # import itertools
    #
    # newlist = list(itertools.chain(*lst))
    # return (newlist)


# lst = [[2,4,3],[1,5,6],[9],[7,9,0]]
# print(flatten_shallow_list(lst))

#24. program to append a list to the second list
# def append_list_two_list(l1, l2):
#     newlist = l1 + l2
#     return newlist
#
# l1 = [1, 2, 3, 0]
# l2 = ['Red', 'Green', 'Black']
# print(append_list_two_list(l1, l2))

#25. Write a Python program to select an
# item randomly from a list.

# lst = ['Red', 'Blue', 'Green', 'White', 'Black']
#
# def random_list_select(lst):
#     from random import randint
#
#     rand_num = randint(0, (len(lst) - 1))
#     return lst[rand_num]
#
# print(random_list_select(lst))

#26. Write a python program to check
#whether two lists are circularly identical.
# def two_cir_idt_list(l1, l2):
#     print('Compare l1 and l2')
#     print(' '.join(map(str, l2)) in ' '.join(map(str, l1* 2)))
#     print('Compare l1 and l3')
#     print(' '.join(map(str, l3)) in ' '.join(map(str, l1 * 2)))
#
# l1 = [0, 0, 10, 10, 10]
# l2 = [10, 10, 10, 0, 0]
# l3 = [1, 10, 10, 0, 0]
# two_cir_idt_list(l1, l2)

#27. Write a Python program to find the
# second smallest number in a list.
# the next two problems use these list, just change name to list and run
# l1 = [1,2,3,4,4]
# l2 = [1, 1, 1, 0, 0, 0, 2, -2, -2]
# l3 = [2,2,5,0,3]
# l4 = [1]

# def second_smallest(list):
#     if len(list) == 0: return ('No list provided.')
#     else:
#         list.sort()
#         return(list[1])
#
# print(second_smallest(list))

#28. Write a Python program to find the
# second largest number in a list.

# def second_largest(list):
#     if len(list) == 0: return ('No list provided.')
#     else:
#         list.sort()
#         return(list[-2])
#
# print(second_largest(list))

#29. Write a Python program to get unique
# values from a list.

# def uniq_values(lst):
#     unique_list = list(dict.fromkeys(lst))
#     if len(lst) == 0: return ('No list provided.')
#     else:
#         return unique_list
#
# print(uniq_values(lst))

#30. Write a Python program to get the frequency
# of the elements in a list

# def freq_values(lst):
#     # import collections
#     # freq = collections.Counter(lst)
#     freq = {}
#     for item in lst:
#         if (item in freq):
#             freq[item] += 1
#         else:
#             freq[item] = 1
#     return freq
#
# print(freq_values(l2))

#31. Write a Python program to count the number
# of elements in a list within a specified range.

# def count_range_in_list(lst, min, max):
#     count = 0
#     for x in lst:
#         if min <= x <= max:
#             count += 1
#     return count

# list1 = [10,20,30,40,40,40,70,80,99]
# print(count_range_in_list(list1, 40, 100))

# list2 = ['a','b','c','d','e','f']
# print(count_range_in_list(list2, 'a', 'e'))

#32. Write a Python program to check whether
# a list contains a sublist.
# def sub_list_in_list(lst, sublst):
#
#     for item in lst:
#         if (' '.join(map(str, item)) in ' '.join(map(str, sublst* 2)) and len(item) == len(sublst)): break
#     return (lst.index(item))
#
#
# lst = [[2,3],[1,1,1],[3,5,3],[1,2,2],[1]]
# sublst = [1,2,2]
# print(sub_list_in_list(lst, sublst))

#33.Write a Python program to generate all sublists of a list
# def sublist_from_list(lst):
#     sublist = [[]]
#     for i in range(len(lst) + 1):
#         for j in range(i + 1, len(lst) + 1):
#             sub = lst[i:j]
#             sublist.append(sub)
#
#     return sublist
#Example
# lst = ['X', 'Y', 'Z']
#sublist = [[], ['X'], ['Y'], ['Z'], ['X', 'Y'], ['X', 'Z'], ['Y', 'Z'], ['X', 'Y', 'Z']]
# print(sublist_from_list(lst))

#34. Write a Python program using Sieve of Eratosthenes
# method for computing primes upto a specified number.

# def calc_prime_till_n(n):
#     non_primes = []
#     primes = []
#     for i in range(2, n+1):
#         if i not in non_primes:
#             primes.append(i)
#             for j in range(i*i, n+1, i):
#                 non_primes.append(j)
#     return primes
#
# print(calc_prime_till_n(100))

