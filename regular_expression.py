#! usr/bin/python3

"""
Review of Regular Expression Matching

1. Import the regex module with import re.

2. Create a Regex object with the re.compile()
 function. (Remember to use a raw string.)

3. Pass the string you want to search into the
 Regex object’s search() method. This returns
  a Match object or use Findall() to find all or use sub() to substitute match.

4. Call the Match object’s group() method to
 return a string of the actual matched text.
"""

#all projects will use this module
import re

#Phone Number and Email Address Extractor
def phoneAndEmailRegEx():
    import pyperclip

    #create a regex for Phone Numbers
    phoneRegex = re.compile(r'''(
    (\d{3}|\(\d{3}\))?  #area code
    (\s|-|\.)?          #seperator
    (\d{3})             #first 3 digits
    (\s|-\.)            #separator
    (\d{4})             #last 4 digits
    (\s*(ext|x|ext.)\s*(\d{2,5}))?  #extension
    )''', re.VERBOSE)
    emailRegex = re.compile(r'''(
    [a-zA-Z0._%+-]+   #username
    @                   #@
    [a-zA-Z0-9.-]+      #domain name
    (\.[a-zA-Z]{2,4})   #dot-something
    )''', re.VERBOSE)

    #Find matches in clipboard text.
    text = str(pyperclip.paste())
    matches = []
    for groups in phoneRegex.findall(text):
        phoneNum = '-'.join([groups[1],groups[3],groups[5]])
        if groups[8] != '':
            phoneNum += ' x' + groups[8]
        matches.append(phoneNum)
    for groups in emailRegex.findall(text):
        matches.append(groups[0])

    #Copy results to the clipboard.
    if len(matches) > 0:
        pyperclip.copy('\n'.join(matches))
        print('Copied to clipboard: ', end="\n\n")
        print('\n'.join(matches))
    else:
        print('No phone numbers or email addresses found.')

#Date Detection
def dateDetection(date):

    #Date Regex
    dateRegEx = re.compile(r'''(
    (\d{2})  #day(01-12)
    /  #seperator
    (\d{2})  #month(01-12)
    /  #seperator
    (\d{4})    #year
    )''', re.VERBOSE)
    matchDate = dateRegEx.search(date)

    #Store values into variables Day, Month, Year
    day = matchDate.group(2)
    month = matchDate.group(3)
    year = matchDate.group(4)

    #Validate Date
    month30Days = ['04','06','09','11']
    leapYear = (not(bool(int(year) % 4)) and (bool(int(year)%100) or not(bool(int(year)%400))))
    if ((month in month30Days) and (int(day) > 30)): return "Invalid Date: There cannot be more than 30 days"
    elif (not(leapYear) and month == '02' and (int(day) > 28)): return "Invalid Date: Feb has 28 days, unless it's a leap year"
    elif (leapYear and (int(day) > 29)): return "Invalid Date: Although a leap year, Feb has no more than 29 days"

    return (day,month,year,leapYear, 'Valid Date format')

date = '29/02/2020'
print(dateDetection(date))