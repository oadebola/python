#1. Write a Python program to calculate
# the length of a string.

# def len_string(string):
#     return len(string)
#
#
# string = "w3resource.com"
# print(len_string(string))

#2. Write a Python program to count the
# number of characters (character frequency) in a string.
# def character_counter(string):
#     freq = {}
#     for ch in string:
#         if ch in freq: freq[ch] += 1
#         else: freq[ch] = 1
#
#     return freq
#
# string = "google.com"
#
# print(character_counter(string))

#3. Write a Python program to get a string made of the
# first 2 and the last 2 chars from a given a string.
# If the string length is less than 2, return instead of the empty string.
# def return_first_last_two_ch(string):
#     if len(string) < 2: return "Empty String"
#     elif len(string) == 2: return(string * 2)
#     else:
#         first_two_ch = string[:2]
#         last_two_ch = string[-2:]
#         #Debug
#         # print(last_two_ch)
#         return first_two_ch + last_two_ch

# string = "w3"
# string = 'w'
# string = 'w3r'
# string = 'w3resource'
# print(return_first_last_two_ch(string))

#4. Write a Python program to get a string
# from a given string where all occurrences
# of its first char have been changed to '$', except the first char itself.
# def rep_ch_with(string, ch_sel, sym):
#     newstring= ""
#     for ch in string:
#         if ch == ch_sel and ch_sel in newstring:
#             newstring += sym
#             continue
#         newstring += ch
#     return newstring
#
#
# string = "restart"
# print(rep_ch_with(string, 't', '$'))

#5. Write a Python program to get a single string
# from two given strings, separated by a space and
# swap the first two characters of each string.
# def string_concat_swap(string):
#     # print(string)
#     # print(string[0])
#     newstring =""
#     fst_2_ch = string[0][:2]
#     sec_2_ch = string[1][:2]
#     newstring = sec_2_ch + string[0][2:] + " " + fst_2_ch + string[1][2:]
#     return newstring
#
# string = 'abc', 'xyz'
# string_concat_swap(string)
# print(string_concat_swap(string))

#6. Write a Python program to add 'ing' at the
# end of a given string (length should be at
# least 3). If the given string already ends
# with 'ing' then add 'ly' instead.
# If the string length of the given string
# is less than 3, leave it unchanged.

# def add_ing_ly_to_end_string(string):
#     if len(string) < 3: return string
#     else:
#         if ((string[-3:]).lower()) == 'ing':
#             string += "ly"
#         else: string += "ing"
#     return string

# string = "abc"
# string = "string"
# print(add_ing_ly_to_end_string(string))

#7. Write a Python program to find the
# first appearance of the substring 'not'
# and 'poor' from a given string, if 'not'
# follows the 'poor', replace the whole
# 'not'...'poor' substring with 'good'.
# Return the resulting string.

# def repl_words(string, str1, str2, chs_word):
#     str_one = string.find(str1)
#     str_two = string.find(str2)
#
#     if (str_two > str_one and str_one > 0 and str_two > 0):
#         string = string.replace(string[str_one : (str_two + len(str2))], chs_word)
#         return string
#     else: return string

# string = "The lyrics is not that poor!"
# string = "The lyrics is poor"

# print(repl_words(string, "not", 'poor', "good"))

#8. Write a Python function that takes a list of words
# and returns the length of the longest one.

# def length_word(string):
#     long_word = ""
#     for word in string:
#         if len(word) > len(long_word):
#             long_word = word
#     return len(long_word)
#
# string = "PHP", "Exercises", "Backend"
# print(length_word(string))

#9. Write a Python program to remove
# the nth index character from a nonempty string.
# def remove_nth_index_ch(string, s):
#     n = s-1
#     ret_string = ""
#     if (s > len(string)): return ("choose is out of range")
#     for i in range(len(string)):
#         if i == n: continue
#         else: ret_string += string[i]
#     return ret_string
#
# string = "python"
# s = 7
# print(remove_nth_index_ch(string, s))

#10. Write a Python program to change a given
# string to a new string where the first and
# last chars have been exchanged.
# def change_first_last_string(string):
#     first_char = string[0]
#     last_char = string[-1]
#     in_btw_string = string[1:-1]
#     return last_char + in_btw_string + first_char
#
# # string = "abcd"
# string = "52341"
#
# print(change_first_last_string(string))

#11. Write a Python program to remove
# the characters which have odd index values of a given string.
# def rem_odd_index_ch(string):
#     ret_string = ""
#     for i in range(len(string)):
#         if i%2 != 0: continue
#         else: ret_string += string[i]
#     return ret_string
#
# # string = "abcdef"
# # string = 'python'
# print(rem_odd_index_ch(string))

#12. Write a Python program to count the
# occurrences of each word in a given sentence.
# def occur_ch(para):
#     list_words = para.split()
#     freq_word = {}
#     for word in list_words:
#         if word in freq_word: freq_word[word] += 1
#         else: freq_word[word] = 1
#     return freq_word
#
# para = "the quick brown fox jumps over the lazy dog"
# print(occur_ch(para))

#13. Write a Python script that takes input from
# the user and displays that input back in upper and lower cases.
# def input_lower_upper():
#     message = input("Write something: ")
#     return(message.upper(), message.lower())
#
# print(input_lower_upper())

#14. Write a Python program that
# accepts a comma separated sequence
# of words as input and prints the unique
# words in sorted form (alphanumerically).
# def sort_words():
#     sample = input("Enter a comma sequence of words: ")
#     lst_sample = sample.split(",")
#     set_sample = set(lst_sample)
#     lst_set_sample = sorted(list(set_sample))
#     string = ", ".join(lst_set_sample)
#     return string
#
# print(sort_words())

#15. Write a Python function to create
# the HTML string with tags around the word(s).
# def add_tags(tag, word):
#     return('<' + tag + '>' + word + '</' + tag +'>')
#
# tag = "br"
# word = "python"
# print(add_tags(tag, word))

#16. Write a Python function to insert
# a string in the middle of a string.
# def insert_str(str, word):
#     idx = (len(str) / 2)
#     pre_word = str[:int(idx)]
#     post_word = str[int(idx):]
#
#     return pre_word + word + post_word
#
#
# word = "html"
# str = "<!----!>"
# str = "((())"
# print(insert_str(str, word))

#17. Write a Python function to get a
# string made of 4 copies of the last
# two characters of a specified string
# (length must be at least 2).

# def multiply_end(word, mlpier):
#     last_two_ch_word = word[-2:]
#     mlp_word = last_two_ch_word * mlpier
#     return mlp_word
#
# word = "python"
# print(multiply_end(word, 4))

#19. Write a Python program
# to get the last part of a string before a specified character.
# def last_part_string(word, chr):
#     last_part_before_chr = word.rsplit(chr)[0]
#     return last_part_before_chr
#
# word = "www.respectyoself.com/self"
# chr = "/"
#
# print(last_part_string(word, chr))

#20. Write a Python function to
# reverses a string if it's length is a multiple of 4.

# def rev_str_more_4(string):
#     if len(string) % 4 == 0:
#         rev = "".join(reversed(string))
#     else: return string
#     return(rev)
#
# string = "Ruby"
# print(rev_str_more_4(string))

#21. Write a Python function to convert a given string
# to all uppercase if it contains at least 2 uppercase
# characters in the first 4 characters.

# def convert_upper_if(string):
#     count = 0
#     newstring = ""
#     for ch in (string[:4]):
#         if ch.isupper(): count += 1
#     if count >= 2:
#         newstring = string.upper()
#     else: return string
#     return newstring
#
#
# string = "PYthon"
# print(convert_upper_if(string))

#22.Write a Python program to sort a string lexicographically.
# def sort_string(string):
#     rev = sorted(string)
#     rev_join = "".join(rev)
#     return rev
#
# # string = "w3resource"
# string = "quickbrown"
# print(sort_string(string))

#23. Write a Python program to remove a newline in Python.
# def rem_newline(string):
#     newline_rem = string.rstrip()
#     # print(string)
#     return newline_rem
#
# string = "Python Exercises\n"
# print(rem_newline(string))

#24. Write a Python program to check
# whether a string starts with specified characters.

# def starts_with(string, check):
#     ans = string.lower().startswith(check)
#     return ans
#
# string = "w3resources.com"
# check = "w3r"
# print(starts_with(string, check))

#25. Write a Python program to create a Caesar encryption.
"""
Note : In cryptography, a Caesar cipher, also known as Caesar's cipher,
the shift cipher, Caesar's code or Caesar shift, is one of the simplest
and most widely known encryption techniques. It is a type of substitution
cipher in which each letter in the plaintext is replaced by a letter
some fixed number of positions down the alphabet. For example, with
a left shift of 3, D would be replaced by A, E would become B, and so on.
The method is named after Julius Caesar, who used it in his private
correspondence.
"""

# def caesar_cipher(input, step):
#     shift = ""
#     alpha = "abcdefghijklmnopqrstuvwxyz"
#     left_3_shift_alpha = ""
#     shifted_alpha = alpha[int(step):] + alpha[:int(step)]
#     print(shifted_alpha)
#     translation = input.maketrans(alpha, shifted_alpha)
#     shift = input.translate(translation)
#     return shift
#
#
# input = "I liKe pie"
# print(caesar_cipher(input, 4))

#27. Write a Python program to remove existing
# indentation from all of the lines in a given text.

# def rem_indent(para):
#     import textwrap
#     mod_para = textwrap.dedent(para)
#     return mod_para
#
#
#
para = """
    Python is a widely used high-level, general-purpose, interpreted,
    dynamic programming language. Its design philosophy emphasizes
    code readability, and its syntax allows programmers to express
    concepts in fewer lines of code than possible in languages such
    as C++ or Java.
"""
# print(rem_indent(para))

#28. Write a Python program to add a prefix
# text to all of the lines in a string.

# def prefix_lines(para):
#     import textwrap
#     mod_para = textwrap.dedent(para)
#     mod_para_final = textwrap.fill(
#         mod_para,
#         width=70,
#         initial_indent="<",
#         subsequent_indent="<"
#     )
#     return mod_para_final
#
# print(prefix_lines(para))

#30. Write a Python program to print the
# following floating numbers upto 2 decimal places.

# def print_upto_2_dec(floater):
#     num = "{:.2f}".format(floater)
#     return num
#
#
# # floater = 3.1415926
# floater = 12.99999
# print(print_upto_2_dec(floater))

#31. Write a Python program to print
# the following floating numbers upto
# 2 decimal places with a sign.
# def ret_float(num):
#     mod_num = "{:+.2f}".format(num)
#     return mod_num

# num = 3.1415926
# num = -12.9999
# print(ret_float(num))

#32. Write a Python program to print
# the following floating numbers with no decimal places.

# def ret_float_no_dec(num):
#     mod_num = "{:+.0f}".format(num)
#     return mod_num
#
# print(ret_float_no_dec(num))

#33. Write a Python program to print
# the following integers with zeros on the left of specified width.
# def add_leading_int(num):
#     return("{:0>6d}".format(num))
#
# num = 3
# print(add_leading_int(num))

# 34. Write a Python program to print
# the following integers with '*' on the right of specified width.
# def add_leading_sym(num):
#     return("{:*<6d}".format(num))
#
# print(add_leading_sym(num))

#35.Write a Python program to display a number with a comma separator.
# def insert_comma(num):
#     return ("{:,}".format(num))

# num = 3100
# num = 30010
# num = 300001
# num = 3000000

# print(insert_comma(num))

#36. Write a Python program to format a number with a percentage.
# def insert_percent(num):
#     return ("{:.3%}".format(num))

# num = 0.1
# num = 0.01
# num = 0.001
# num = 0.0001

# print(insert_percent(num))

#37. Write a Python program to display a number in left,
# right and center aligned of width 10.
# def word_align(word, width):

    # option1 num must be a string
    # return (word.ljust(num),word.center(num), word.rjust(num))

    # option2
    # return ("{:> 10d}".format(num), "{:^ 10d}".format(num), "{:< 10d}".format(num))

# num = 22
# width = 10

# print(word_align(word, width))

#38. Write a Python program to count occurrences of a substring in a string.
# def count_substring(string, sub_s):
    #option 1: count method
    # return (string.count(sub_s))

    #option 2: split
    # count = 0
    # for sub_s in string.split():
    #     count += 1
    #
    # return (count)

# string = "Howdy Howdy oo, Yoody yoody yoody yo"
# sub_s = "yo"

# print(count_substring(string, sub_s))

#39. Write a Python program to reverse a string. Go to the editor
# def rev_string(string):
    # option1: string splice
    # return (string[-1::-1])

    # option2: reversed method
    # return ''.join(reversed(string))

# string = "python"
# print(rev_string(string))

#40. Write a Python program to reverse words in a string.
# def rev_str_words(word):
#
#     for string in word.split("\n"):
#         return (' '.join(string.split()[::-1]))
#
# word = "The quick brown fox"
# print(rev_str_words(word))

#41. Write a Python program to strip a set of characters from a string.

# def strip_character(sent, chars):
#     return "".join(ch for ch in sent if ch not in chars)
#
# sent = "The quick brown fox"
# chars = "aeiou"
#
# print(strip_character(sent, chars))

#42. Write a Python program to count repeated characters in a string.
# def count_chars(string):
#     freq_count = {}
#     freq1_count = {}
#     for ch in string:
#         if ch in freq_count: freq_count[ch] += 1
#         else: freq_count[ch] = 1
#
#     for chs in freq_count:
#         if freq_count[chs] >= 2: freq1_count.update({chs : freq_count[chs]})
#
#     return freq1_count
#
# string = "thequickbrownfoxjumpsoverthelazydog"
# print(count_chars(string))

#43. Write a Python program to print the square
# and cube symbol in the area of a rectangle
# and volume of a cylinder.

# def print_area_vol(area, vol, dec):
#     return ("The area is {:.{}f}cm\u00b2".format(area, dec), "\n", "The volume is {:.{}f}cm\u00b3".format(vol,dec) )
#
# area = 1256.66
# vol = 1254.725
# dec = 2
#
# print(print_area_vol(area,vol,dec))

#44. Write a Python program to print the
# index of the character in a string.

# def ret_idx_ch(string):
#     for idx, ch in enumerate(string):
#         print ("Char {} is at position {}".format(ch, idx))
#
# string = "w3resource"
#
# ret_idx_ch(string)

#45. Write a Python program to check
# if a string contains all letters of the alphabet.

# def all_lets_alp(input_str):
#     import string
#     alphabet = set(string.ascii_lowercase)
#     return (set(input_str.lower()) >= alphabet)
#
#
# input_str = "The quick brown fox jumps over the lazy cat"
# input_str = "The quick brown fox jumps over the lazy dog"
# print(all_lets_alp(input_str))

#46. Write a Python program to convert a string in a list.
# def string_to_list(string):
#     return (string.split())
#
# string = "This is a test string"
# print(string_to_list(string))

#47. Write a Python program to lowercase first
# n characters in a string.

# def lowercase_n_string(string, n):
    #option1: tedious
    # ret_str = ""
    # for i in range(len(string)):
    #     if (i < n): ret_str += string[i].lower()
    #     else: ret_str += string[i]
    # return ret_str
    #option2
    # return (string[:n].lower() + string[n:])


# string = "W3RESOURCE.COM"
# n = 4
# print(lowercase_n_string(string, n))

#48. Write a Python program to swap comma
# and dot in a string.
# def rep_comma_dot(str):
#     return (str.translate(str.maketrans(".,",",.")))
#
# str = "32.054,23"
# print(rep_comma_dot(str))

#49. Write a Python program to count and
# display the vowels of a given text.
# def count_n_string(string, n):
#     return (len([letter for letter in string if letter in vowels]))
#
# string = "Welcome to w3resource.com"
# vowels = "aeiouAEIOU"
# print(count_n_string(string, n = vowels))

#50. Write a Python program to split a string
# on the last occurrence of the delimiter.

# def split_string(string, n):
#     return (string.rsplit(',', n))
#
# string = "w,3,r,e,s,o,u,r,c,e"
# print(split_string(string, n= 3))

#51. Write a Python program to find the
# first non-repeating character in given string.
# def ret_first_non_repeat_ch(string):
#     ch_map = {}
#     ch_order = []
#     for ch in string:
#         if ch in ch_map: ch_map[ch] += 1
#         else:
#             ch_map[ch] = 1
#             ch_order.append(ch)
#
#     for key in ch_order:
#         if (ch_map[key] == 1):
#             return key
#
#     # return ch_map, ch_order
#
# string = "aabbccd"
# print(ret_first_non_repeat_ch(string))

#52. Write a Python program to print all
# permutations with given repetition
# number of characters of a given string.
# def all_combos(string, rp):
#     from itertools import product
#     str_lst = list(string)
#     result = []
#     for c in product(str_lst, repeat = rp):
#         result.append(c)
#     return result
#
# print(all_combos("abcd", 2))

#53. Write a Python program to find the
# first repeated character in a given string.
# def ret_first_repeat_ch(string):
    #option1: basic for loop with freq counter
    # ch_map = {}
    # first_repeat = []
    # for ch in string:
    #     if ch in ch_map:
    #         ch_map[ch] += 1
    #         first_repeat.append(ch)
    #         break
    #     else: ch_map[ch] = 1
    # return first_repeat

    #option2:
    # for index,c in enumerate(string):
    #     if string[:index+1].count(c) > 1:
    #         return c
    #     return "None"

# string = "abcdefghfijklabc"
# print(ret_first_repeat_ch(string))

#56. Write a Python program to find
# the second most repeated word in a given string.
# def ret_second_most_rep_word(sent):
#     sent_list = sent.split()
#     word_dict = {}
#     for word in sent_list:
#         if word in word_dict: word_dict[word] += 1
#         else: word_dict[word] = 0
#
#     sort_word_dict = sorted(word_dict.items(), key=lambda kv: kv[1])
#     return sort_word_dict
#
# sent = """
# I felt happy because i saw the others were
# happy and because I knew I should feel happy,
# but I wasn't really happy.
# """
# print(ret_second_most_rep_word(sent))

#57.Write a Python program to remove
# spaces from a given string.
# def rem_spaces(string):
#     string = string.replace(' ', '')
#     return string
#
# string = 'w 3 r es ources'
# print(rem_spaces(string))

#58. Write a Python program to move
# spaces to the front of a given string.
# def move_Spaces_front(string):
#   noSpaces_char = [ch for ch in string if ch!=' ']
#   spaces_char = len(string) - len(noSpaces_char)
#   result = ' '*spaces_char + ''.join(noSpaces_char)
#   return(result)


# string = "w3resource .  com  "
# string = "   w3resource.com  "
# print(move_Spaces_front(string))





