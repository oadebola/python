import random

def coinFlip(n, s):

    flipList = []
    streaks = 0
    p1 = 0
    p2 = 0

    #generate flips and add to list
    for flip in range(n):
        if random.randint(0,1) == 0: flipList.append("H")
        else: flipList.append("T")
    print("debug flipList: ", flipList)

    #check for s consecutive flips and increase streak
    # flipList = ['T', 'H', 'H', 'T', 'H', 'H', 'H', 'H', 'H', 'H']
    currStreak = 0

    # print("length of flipList", len(flipList))
    while p1 < int((len(flipList) - 1)):

        if p2 >= (len(flipList)): break

        if flipList[p1] == flipList[p2]:
            # print('if')
            # print(flipList[p1] + " : " + flipList[p2])
            currStreak += 1
            p2 += 1
            # print("currStreak is: ", currStreak)

            if currStreak == s:
                streaks += 1
                p1 = p2
                currStreak = 0

        else:
            # print('else')
            # print(flipList[p1] + " : " + flipList[p2])
            currStreak = 0
            # print("currStreak is: ", currStreak)
            p1 = p2

    print("Streak: ", streaks)




coinFlip(10, 3)
