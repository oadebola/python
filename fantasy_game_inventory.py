inventory = {
    'rope': 1,
    'torch': 6,
    'gold coin': 42,
    'dagger': 1,
    'arrow': 12
}

def display_inventory(inventory):
    print('Inventory:')
    sum_of_items = 0
    for item in inventory:
        print('%d %s'%(inventory[item], item))
        sum_of_items += inventory[item]
    print('Total number of items: %d'%(sum_of_items))

dragonLoot = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']
def addToInventory(inventory, addedItems):
    for item in addedItems:
        if item in inventory: inventory[item] += 1
        else:
            inventory[item] = 1
    display_inventory(inventory)

addToInventory(inventory, dragonLoot)